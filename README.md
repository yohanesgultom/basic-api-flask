# Basic API Flask

Development (Unix-like):
1. Install dependencies `pip install -r requirements.txt`
2. Run test `pytest -v`
3. Run `FLASK_ENV=development FLASK_DEBUG=1 flask run`

Production (Unix-like):
1. Install dependencies `pip install -r requirements.txt`
2. Install uWSGI (Ubuntu 18.04 LTS: `apt install uwsgi-plugin-python3`)
3. Run on: 
   * File Socket (faster) `uwsgi -s /tmp/basic-api-flask.sock module wsgi:app` 
   * HTTP socket port xxxx `uwsgi --http-socket :xxxx module wsgi:app` 

> If you use virtual environment just add `--virtualenv /path/to/env`. Find detailed guide at https://flask.palletsprojects.com/en/1.1.x/deploying/uwsgi/