from flask import Flask, request
from werkzeug.utils import secure_filename
from werkzeug.exceptions import HTTPException
import os, json, datetime, urllib.request

app = Flask(__name__)

@app.errorhandler(HTTPException)
def handle_http_exception(e):
    """Return JSON instead of HTML for HTTP errors"""
    response = e.get_response()
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response

@app.errorhandler(Exception)
def handle_exception(e):
    """Return JSON instead of HTML for other errors"""
    app.logger.exception(e)
    res = {
        "code": 500,
        "name": type(e).__name__,
        "description": str(e),
    }
    return res, 500


@app.route('/', methods=['GET'])
def index():
    app.logger.debug('This is a debug message')
    return {'message': 'Hello world'}


@app.route('/holidays/<country_code>', methods=['GET'])
def holidays(country_code='ID'):    
    year = request.args.get('year', datetime.datetime.today().year)
    api_url = f'https://date.nager.at/api/v2/PublicHolidays/{year}/{country_code}'
    req = urllib.request.Request(api_url)
    with urllib.request.urlopen(req) as response:
        res = response.read()
        data = json.loads(res)
    return {'data': data}
    

@app.route('/upload', methods=['POST'])
def upload_file():
    for key in request.files:
        f = request.files[key]
        filename = f'{key}-{secure_filename(f.filename)}'
        f.save(os.path.join('upload', filename))
    return {'message': 'File(s) uploaded'}