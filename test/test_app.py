import pytest, json
from pytest_mock import mocker

from app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_index(client):
    res = client.get('/')
    data = res.get_json()
    assert data['message'] == 'Hello world'

def test_holidays(client, mocker):
    expected = [
        {
            "counties": None, 
            "countryCode": "US", 
            "date": "2015-01-01", 
            "fixed": False, 
            "global": True, 
            "launchYear": None, 
            "localName": "New Year's Day", 
            "name": "New Year's Day", 
            "type": "Public",
        }
    ]    
    mock_res = mocker.MagicMock()
    mock_res.getcode.return_value = 200
    mock_res.read.return_value = json.dumps(expected)
    mock_res.__enter__.return_value = mock_res
    mock_request = mocker.patch('urllib.request')
    mock_request.urlopen.return_value = mock_res

    res = client.get('/holidays/ID')
    data = res.get_json()
    assert len(data['data']) == len(expected)
    assert data['data'][0] == expected[0]
